import React, {FC, useCallback, useContext, useEffect, useState} from 'react';
import {TextInput, View} from 'react-native';
import {SheetManager} from 'react-native-actions-sheet';
import Pdf from 'react-native-pdf';
import Share from 'react-native-share';
import Toast from 'react-native-toast-message';
import {FilesContext} from '../../screens/Home';
import Button from '../Button';
import Sheet from '../Sheet';

import ShareIcon from '../../assets/share.svg';
import SaveIcon from '../../assets/save.svg';
import DeleteIcon from '../../assets/delete.svg';
import styles from './Preview.style';

import {SheetUtilsType} from '../Sheet/Sheet';

type PreviewPropsType = {
  documentUri: string;
  id: string;
  trigger?: FC<SheetUtilsType>;
  clear?: () => void;
};

const Preview: FC<PreviewPropsType> = ({
  documentUri,
  id,
  trigger,
  clear = () => {},
}) => {
  const [name, setName] = useState('Untitled');
  const {files, addFile} = useContext(FilesContext);

  useEffect(() => {
    if (!trigger) {
      SheetManager.show(id);
    }
  }, [id, trigger]);

  const share = useCallback(() => {
    Share.open({url: documentUri});
  }, [documentUri]);

  const save = useCallback(() => {
    const isExisted = files?.[name];
    if (isExisted) {
      return Toast.show({
        type: 'info',
        text1: 'EXISTED',
        text2: "Name already taken, let's change it.",
      });
    }
    addFile(name, documentUri);
    SheetManager.hide(id);
  }, [addFile, documentUri, files, id, name]);

  return (
    <Sheet
      id={id}
      trigger={trigger}
      onClose={clear}
      render={({close}) => (
        <View>
          <View style={styles.preview}>
            <Pdf source={{uri: documentUri}} style={styles.pdf} />
          </View>
          <TextInput
            value={name}
            onChangeText={text => {
              setName(text);
            }}
            style={styles.input}
          />
          <View style={styles.controllers}>
            <Button
              text="Share"
              icon={ShareIcon}
              type="secondary"
              style={styles.margin}
              onPress={share}
            />
            <Button
              text="Save"
              icon={SaveIcon}
              type="secondary"
              style={styles.margin}
              onPress={save}
            />
            <Button
              text="Delete"
              icon={DeleteIcon}
              type="danger"
              onPress={close}
            />
          </View>
        </View>
      )}
    />
  );
};

export default Preview;
